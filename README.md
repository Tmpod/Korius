[![Power by Kotlin](https://img.shields.io/static/v1?label=Powered+by&message=Kotlin&color=ec7741&logo=kotlin&style=flat-square)](https://kotlinlang.org) [![Built with Strife](https://img.shields.io/static/v1?label=Built+with&message=Strife&color=00b1ff&style=flat-square)](https://gitlab.com/serebit/strife) [![Built with Strife](https://img.shields.io/static/v1?label=Built+with&message=Koin&color=f69106&style=flat-square)](https://insert-koin.io)
# Korius

**Korius** is a multipurpose **[Discord](https://discordapp.com)** bot written in Kotlin with the aid of **[Strife](https://gitlab.com/serebit/strife)** and **[Koin](https://insert-koin.io)**.

> :warning: This project is still heavily under development! Stay tuned for updates!
 
If you have any question or suggestion, please open an issue or contact me via Discord (Tmpod#1933).