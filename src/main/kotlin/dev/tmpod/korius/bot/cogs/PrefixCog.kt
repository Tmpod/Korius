/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.bot.cogs

import com.serebit.strife.entities.Message
import com.serebit.strife.events.MessageCreateEvent
import dev.tmpod.korius.framework.api.dsl.group
import dev.tmpod.korius.framework.api.dsl.listener
import dev.tmpod.korius.framework.core.Cog
import dev.tmpod.korius.framework.core.CommandRegistryConfig
import dev.tmpod.korius.framework.database.GuildConfigRepo
import dev.tmpod.korius.framework.database.getPrefix
import dev.tmpod.korius.framework.database.setPrefix
import org.koin.core.inject

/** Maximum length a custom prefix can be */
const val MAX_PREFIX_LENGTH = 4

class PrefixCog : Cog() {
    private val guildConfigRepo: GuildConfigRepo by inject()
    private val cmdRegistryConfig: CommandRegistryConfig by inject()

    private suspend fun showPrefix(message: Message) {
        val guildPrefix = message.guild?.getPrefix(guildConfigRepo)
                ?: cmdRegistryConfig.defaultPrefix
        message.replyInfo("Hey there!", "My current prefix for this guild is **`$guildPrefix`**!")
    }

    override val commands = listOf(
            group("prefix", "pf", brief = "Manage the command prefix") {
                showPrefix(message)
            }
                    .subCommand("get", "g", brief = "Gets the current prefix") {
                        showPrefix(message)
                    }
                    .subCommand("set", "s", brief = "Sets the prefix") {
                        when {
                            args.isEmpty() -> run { message.replyError("No prefix passed!"); return@subCommand }
                            args.size > 1 -> run { message.replyError("Cannot set multiple prefixes!"); return@subCommand }
                        }

                        val newPrefix = args[0]
                        if (newPrefix.length > MAX_PREFIX_LENGTH) {
                            message.replyError("Prefix cannot be longer than `$MAX_PREFIX_LENGTH` characters!")
                        } else {
                            message.guild!!.setPrefix(guildConfigRepo, newPrefix)
                            message.replySuccess("Successfully set the prefix to **`$newPrefix`**!")
                        }
                    }
                    .subCommand("reset", "r", brief = "Resets the prefix back to the default") {
                        if (args.isNotEmpty()) {
                            message.replyError("This command takes no arguments!")
                        } else {
                            message.guild!!.setPrefix(guildConfigRepo, cmdRegistryConfig.defaultPrefix)
                            message.replySuccess("Successfully reset the prefix to **`${cmdRegistryConfig.defaultPrefix}`**!")
                        }
                    }
    )

    override val listeners = listOf(
            listener<MessageCreateEvent> {
                if (message.content.trim() in arrayOf(context.selfUser.asMention, "<@!${context.selfUser.id}>")) {
                    showPrefix(message)
                }
            }
    )
}
