/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.bot

import com.serebit.strife.BotBuilder
import com.serebit.strife.onMessageCreate
import dev.tmpod.korius.KoriusAppConfig
import org.koin.dsl.module

val botModule = module {
    single { BotBuilder(getProperty<BotConfig>("botConfig").token) }
    single { getProperty<CustomEmojisConfig>("emojisConfig") }
}
