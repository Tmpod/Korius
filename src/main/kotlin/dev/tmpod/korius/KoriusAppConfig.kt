/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius

import dev.tmpod.korius.bot.BotConfig
import dev.tmpod.korius.bot.CustomEmojisConfig
import dev.tmpod.korius.framework.core.CommandRegistryConfig
import dev.tmpod.korius.framework.database.DatabaseConfig

data class KoinConfig(val loggingLevel: String)

data class KoriusAppConfig(
        val koin: KoinConfig,
        val database: DatabaseConfig,
        val bot: BotConfig,
        val emojis: CustomEmojisConfig,
        val commandRegistry: CommandRegistryConfig
)
