/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius

import com.serebit.strife.BotBuilder
import dev.tmpod.korius.bot.BotConfig
import dev.tmpod.korius.bot.cogs.PingCog
import dev.tmpod.korius.bot.cogs.PrefixCog
import dev.tmpod.korius.framework.core.CogLoader
import dev.tmpod.korius.framework.database.GuildConfigRepo
import org.koin.core.KoinComponent
import org.koin.core.inject
import kotlin.time.ExperimentalTime

class KoriusApp : KoinComponent {
    private val guildConfigRepo: GuildConfigRepo by inject()
    private val cogLoader: CogLoader by inject()
    private val botBuilder: BotBuilder by inject()
    private val botConfig = getKoin().getProperty<BotConfig>("botConfig")

    suspend fun start() {
        // TODO I need to change this into a more dynamic loading process, maybe with scripting
        cogLoader.loadCogs(PingCog(), PrefixCog())

        botBuilder.build()?.connect()
    }
}
