/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.utils

// Citation: https://en.wikipedia.org/wiki/Quotation_mark
private val DEFAULT_QUOTE_MAPPING = mapOf(
//        "```" to "```",
//        "``" to "''",
//        "‛‛" to "’’",
        '`' to '`',
        '‛' to '’',
        '\'' to '\'',
        '"' to '"',
        '“' to '”',
        '‘' to '’',
        '«' to '»',
        '‹' to '›',
        '《' to '》',
        '〈' to '〉',
        '「' to '」',
        '｢' to '｣',
        '『' to '』',
        '〝' to '〞',
        '＂' to '＂',
        '＇' to '＇'
)
//private const val ESCAPE_CHAR = '\\'
//private const val SEPARATOR = ' '

/*
 * Splits a string into bits taking quotes and escaping into account.
 * Thanks a bunch to Espy for helping me out with this.
 */
fun splitByQuotes(
        string: String,
        amount: Int? = null,  // Amount of bits to get
        escapeChar: Char = '\\',
        separatorChar: Char = ' ',
        quotes: Map<Char, Char> = DEFAULT_QUOTE_MAPPING
): List<String> {
    var currentCloseQuote: Char? = null
    var lastCharWasEscape: Boolean = false
    val parsedArgs = mutableListOf<String>()
    val buffer = StringBuilder()

    for (c in string) {
        if (lastCharWasEscape) {
            lastCharWasEscape = false
            buffer.append(c)
            continue
        }

        if (c == escapeChar) {
            lastCharWasEscape = true
            continue
        }

        if (currentCloseQuote == null) {
            currentCloseQuote = quotes[c]

            if (currentCloseQuote != null) {
                continue
            }

//            if (c.isWhitespace()) {  // Not using this for right now
            if (c == separatorChar) {
                // If we get several spaces in a row, don't treat them as lots of empty string arguments.
                if (buffer.isNotEmpty()) {
                    parsedArgs += buffer.toString()
                    buffer.clear()
                }
            } else {
                buffer.append(c)
            }

        } else if (c == currentCloseQuote) {
            parsedArgs += buffer.toString()
            buffer.clear()
            currentCloseQuote = null
        } else {
            buffer.append(c)
        }

        // Stop parsing if we got the amount we need
        if (parsedArgs.size == amount) {
            break
        }
    }

    // If we hit end of string, this is likely filled, so flush once more if so.
    if (buffer.isNotEmpty()) {
        parsedArgs += buffer.toString()
    }

    require(currentCloseQuote == null) { "Expected a closing $currentCloseQuote" }

    return parsedArgs.toList()
}
