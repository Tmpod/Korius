/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius

import com.sksamuel.hoplite.ConfigLoader
import dev.tmpod.korius.bot.botModule
import dev.tmpod.korius.framework.core.coreModule
import dev.tmpod.korius.framework.database.databaseModule
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import java.io.File
import kotlin.time.ExperimentalTime

suspend fun main(args: Array<String>) {
    println("Hello world!")

    startKoin {
        val config = ConfigLoader().loadConfigOrThrow<KoriusAppConfig>(
                File(args.getOrNull(0)
                        ?: throw RuntimeException("Please provide a path to the config file!")).toPath()
        )

        properties(mapOf(
                "config" to config,
                "dbConfig" to config.database,
                "koinConfig" to config.koin,
                "botConfig" to config.bot,
                "emojisConfig" to config.emojis,
                "commandRegistryConfig" to config.commandRegistry
        ))

        printLogger(Level.valueOf(config.koin.loggingLevel))

        modules(listOf(coreModule, databaseModule, botModule))
    }

    KoriusApp().start()
}
