/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.api.dsl

import com.serebit.strife.entities.Message
import dev.tmpod.korius.framework.commands.Check
import dev.tmpod.korius.framework.commands.CommandLogic
import dev.tmpod.korius.framework.core.Command
import dev.tmpod.korius.framework.core.CommandGroup

fun isValidCommandName(name: String?): Boolean =
        name?.trim()?.length ?: 0 < Message.MAX_LENGTH

fun command(
        name: String,
        vararg aliases:
        String = arrayOf(),
        brief: String? = null,
        description: String? = null,
        usage: String? = null,
        checks: Set<Check> = setOf(),
        logic: CommandLogic
): Command {
    arrayOf(name, *aliases).forEach {
        if (!isValidCommandName(it)) {
            throw RuntimeException("Command name `$it` is invalid!")
        }
    }
    return Command(
            name = name,
            aliases = aliases.toSet(),
            brief = brief,
            description = description,
            usage = usage,
            checks = checks,
            logic = logic
    )
}

fun group(
        name: String,
        vararg aliases:
        String = arrayOf(),
        brief: String? = null,
        description: String? = null,
        usage: String? = null,
        checks: Set<Check> = setOf(),
        alwaysInvoke: Boolean = false,
        logic: CommandLogic
): CommandGroup {
    arrayOf(name, *aliases).forEach {
        if (!isValidCommandName(it)) {
            throw RuntimeException("Command name `$it` is invalid!")
        }
    }
    return CommandGroup(
            name = name,
            aliases = aliases.toSet(),
            brief = brief,
            description = description,
            usage = usage,
            checks = checks,
            alwaysInvoke = alwaysInvoke,
            logic = logic
    )
}
