/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.database

import com.serebit.strife.entities.Guild
import org.litote.kmongo.SetTo
import org.litote.kmongo.set

suspend fun Guild.getConfig(repo: GuildConfigRepo) = repo.findById(this.id)

suspend fun Guild.getPrefix(repo: GuildConfigRepo) = getConfig(repo)?.prefix

suspend fun Guild.setPrefix(repo: GuildConfigRepo, newPrefix: String) = repo.updateOne(id, set(SetTo(GuildConfigDoc::prefix, newPrefix)))
