/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.database

import com.mongodb.client.model.UpdateOptions
import org.bson.conversions.Bson
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.CoroutineDatabase
import org.litote.kmongo.eq

class GuildConfigRepo : KoinComponent {
    private val database: CoroutineDatabase by inject()
    private val collection: CoroutineCollection<GuildConfigDoc> = database.getCollection()

    fun find(filter: Bson) = collection.find(filter)

    suspend fun findById(id: Long) = collection.findOneById(id)

    suspend fun insertNew(doc: GuildConfigDoc) = collection.insertOne(doc)

    suspend fun updateOne(id: Long, update: Bson) = collection.updateOne(
            GuildConfigDoc::id eq id, update, UpdateOptions().upsert(true)
    )

    suspend fun replaceOne(id: Long, newDoc: GuildConfigDoc) = collection.replaceOneById(id, newDoc)
}
