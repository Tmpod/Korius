/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.commands.exceptions

import dev.tmpod.korius.framework.commands.Context

abstract class InvocationFailureException : BaseCommandException {
    val context: Context

    constructor(context: Context) : super() {
        this.context = context
    }

    constructor(context: Context, reason: String) : super(reason) {
        this.context = context
    }

    constructor(context: Context, cause: Throwable?) : super(cause) {
        this.context = context
    }

    constructor(context: Context, reason: String, cause: Throwable?) : super(reason, cause) {
        this.context = context
    }
}
