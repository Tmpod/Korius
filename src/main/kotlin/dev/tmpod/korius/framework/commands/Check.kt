/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.commands

import dev.tmpod.korius.framework.commands.exceptions.CheckFailureException

data class Check(
        val name: String,
        val logic: (suspend (Context) -> Boolean)
) {
    suspend fun run(context: Context) {
        val result = try {
            logic(context)
        } catch (ex: Exception) {
            throw CheckFailureException(context, ex)
        }

        if (!result) {
            throw CheckFailureException(context)
        }
    }
}
