package dev.tmpod.korius.framework.commands

typealias CommandLogic = suspend Context.() -> Unit

interface BaseCommand {
    val name: String
    val aliases: Set<String>
    val description: String?
    val brief: String?
    val usage: String?
    val parent: BaseCommand?
    val checks: Set<Check>
    val logic: CommandLogic

    val fullyQualifiedName: String
        get() = parent?.fullyQualifiedName?.plus(name) ?: name

    val names: Set<String>
        get() = setOf(name).union(aliases)

    suspend fun invoke(context: Context)

    suspend fun runChecks(context: Context)
}
