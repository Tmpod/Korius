/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.core

import dev.tmpod.korius.framework.commands.BaseCommand
import dev.tmpod.korius.framework.commands.Check
import dev.tmpod.korius.framework.commands.CommandLogic
import dev.tmpod.korius.framework.commands.Context

data class Command(
        override val name: String,
        override val aliases: Set<String> = setOf(),
        override val description: String? = null,
        override val brief: String? = null,
        override val usage: String? = null,
        override val parent: BaseCommand? = null,
        override val checks: Set<Check> = setOf(),
        override val logic: CommandLogic
) : BaseCommand {
    override suspend fun invoke(context: Context) {
        runChecks(context)
        logic(context)
    }

    override suspend fun runChecks(context: Context) {
        checks.forEach { it.run(context) }
        // Will implement this later
//        val failedChecks: MutableSet<Check> = mutableSetOf()

//        checks.forEach {
//            try {
//                it.run(context)
//            } catch (ex: CheckFailureException) {
//                failedChecks.add(it)
//            }
//        }
//
//        if (failedChecks.isNotEmpty()) { throw MultiCheckFailureException }
    }
}