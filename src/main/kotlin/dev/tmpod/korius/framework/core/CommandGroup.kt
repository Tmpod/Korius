/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.core

import com.serebit.strife.events.MessageCreateEvent
import dev.tmpod.korius.framework.api.dsl.isValidCommandName
import dev.tmpod.korius.framework.commands.*
import dev.tmpod.korius.framework.commands.exceptions.CommandNotFoundException
import dev.tmpod.korius.utils.splitByQuotes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KLogger
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*

class CommandGroup(
        override val name: String,
        override val aliases: Set<String> = setOf(),
        override val description: String? = null,
        override val brief: String? = null,
        override val usage: String? = null,
        override val parent: BaseCommand? = null,
        override val checks: Set<Check> = setOf(),
        override val logic: CommandLogic = { },
        val alwaysInvoke: Boolean = false
) : BaseCommandRegistry(), BaseCommand, KoinComponent {
    private val _commands = mutableSetOf<BaseCommand>()
    private val _commandNameMap = WeakHashMap<String, BaseCommand>()
    override val logger = KotlinLogging.logger {}

    val cmdRegistryConfig: CommandRegistryConfig by inject()

    override val commands: Set<BaseCommand>
        get() = _commands.toSet()

    override val commandNameMap: Map<String, BaseCommand>
        get() = _commandNameMap.toMap()

    override val commandNames: Set<String>
        get() = _commands.map { it.name }.union(_commandNameMap.keys)

    override fun addCommand(command: BaseCommand) {
        when {
//            command.parent == null -> throw RuntimeException("You can't add root commands!")
            command in this -> throw RuntimeException("Command `$command` is already registered!")
            command.names.firstOrNull { it in commandNames } != null -> throw RuntimeException("There's already a registered command for that name/alias!")
        }

        _commands.add(command)
        logger.info("Added command: `$command`")

        command.names.forEach {
            _commandNameMap[it] = command
            logger.debug("Registered name/alias `$it` for command `${command.name}`")
        }
    }

    override fun removeCommand(command: BaseCommand) {
        _commands.removeIf { it == command }
        logger.info("Removed command: `$command`")
    }

    override fun clearCommands() {
        _commands.clear()
        logger.info("Cleared commands!")
    }

    fun subCommand(
            name: String,
            vararg aliases:
            String = arrayOf(),
            brief: String? = null,
            description: String? = null,
            usage: String? = null,
            checks: Set<Check> = setOf(),
            logic: CommandLogic
    ): CommandGroup {
        arrayOf(name, *aliases).forEach {
            if (!isValidCommandName(it)) {
                throw RuntimeException("Command name `$it` is invalid!")
            }
        }
        addCommand(
                Command(
                        name = name,
                        aliases = aliases.toSet(),
                        brief = brief,
                        description = description,
                        usage = usage,
                        checks = checks,
                        logic = logic,
                        parent = this
                )
        )

        // Returns the group in order to allow chain calls
        return this
    }

    override suspend fun processCommands(event: MessageCreateEvent, prefix: String, rawArgs: String) {
        if (alwaysInvoke) {
            invoke(Context(event = event, prefix = prefix, rawArgs = rawArgs))
        }

        val cmdCandidateName = splitByQuotes(rawArgs, 1).getOrNull(0)

        if (cmdCandidateName == null) {
            // Might tweak this in the future
            if (!alwaysInvoke) {
                invoke(Context(event = event, prefix = prefix, rawArgs = rawArgs))
            }
            return
        }

        val cmdCandidate = _commandNameMap[cmdCandidateName]
                ?: throw CommandNotFoundException(
                        Context(
                                event = event,
                                prefix = prefix,
                                rawArgs = rawArgs,
                                args = listOf()),
                        cmdCandidateName
                )

        when (cmdCandidate) {
            is CommandGroup -> cmdCandidate.processCommands(
                    event, prefix, rawArgs.substringAfter(cmdCandidateName).trimStart()
            )
            is Command -> GlobalScope.launch {
                cmdCandidate.invoke(
                        Context(
                                event = event,
                                prefix = prefix,
                                rawArgs = rawArgs.substringAfter(cmdCandidateName).trimStart()
                        )
                )
            }
        }
    }

    override suspend fun invoke(context: Context) {
        runChecks(context)
        logic(context)
    }

    override suspend fun runChecks(context: Context) {
        checks.forEach { it.run(context) }
    }
}
