/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.core

import com.serebit.strife.BotBuilder
import com.serebit.strife.events.MessageCreateEvent
import dev.tmpod.korius.framework.commands.BaseCommand
import dev.tmpod.korius.framework.commands.BaseCommandRegistry
import mu.KotlinLogging
import org.koin.core.KoinComponent
import org.koin.core.inject
import com.serebit.strife.onMessageCreate
import dev.tmpod.korius.framework.commands.Context
import dev.tmpod.korius.framework.commands.exceptions.CommandNotFoundException
import dev.tmpod.korius.framework.database.GuildConfigRepo
import dev.tmpod.korius.framework.database.getPrefix
import dev.tmpod.korius.utils.splitByQuotes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.WeakHashMap

// TODO add a coro scope for this class

class CommandRegistry : BaseCommandRegistry(), KoinComponent {
    private val config: CommandRegistryConfig by inject()
    private val botBuilder: BotBuilder by inject()
    private val guildConfigRepo: GuildConfigRepo by inject()

    private val _commands = mutableSetOf<BaseCommand>()
    private val _commandNameMap = WeakHashMap<String, BaseCommand>()
    override val logger = KotlinLogging.logger {}

    init {
        botBuilder.apply { onMessageCreate { processCommands(this) } }
    }

    override val commands: Set<BaseCommand>
        get() = _commands.toSet()

    override val commandNameMap: Map<String, BaseCommand>
        get() = _commandNameMap.toMap()

    override val commandNames: Set<String>
        get() = _commands.map { it.name }.union(_commandNameMap.keys)

    override operator fun contains(command: BaseCommand) = command in _commands

    override fun addCommand(command: BaseCommand) {
        when {
            command.parent != null -> throw RuntimeException("You can only add root commands!")
            command in this -> throw RuntimeException("Command `$command` is already registered!")
            command.names.firstOrNull { it in commandNames } != null -> throw RuntimeException("There's already a registered command for that name/alias!")
        }

        _commands.add(command)
        logger.info("Added command: `$command`")

        command.names.forEach {
            _commandNameMap[it] = command
            logger.debug("Registered name/alias `$it` for command `${command.name}`")
        }
    }

    override fun removeCommand(command: BaseCommand) {
        _commands.removeIf { it == command }
        logger.info("Removed command: `$command`")
    }

    override fun clearCommands() {
        _commands.clear()
        logger.info("Cleared commands!")
    }

    override suspend fun processCommands(event: MessageCreateEvent, prefix: String, rawArgs: String) {
        val msg = event.message

        val guildPrefixCandidate = msg.guild?.getPrefix(guildConfigRepo) ?: config.defaultPrefix
        val prefixCandidates = if (config.allowMentionsAsPrefix) arrayOf(
                guildPrefixCandidate,
                msg.context.selfUser.asMention,
                "<@!${msg.context.selfUser.id}>"  // nick mention
        ) else arrayOf(guildPrefixCandidate)

        val actualPrefix = prefixCandidates
                .filter { msg.content.startsWith(it, config.ignorePrefixCase) }
                .getOrElse(0) { return }

        val cmdCandidateName = splitByQuotes(msg.content.drop(actualPrefix.length), 1).getOrElse(0) { return }
        val cmdCandidate = _commandNameMap[cmdCandidateName]
                ?: throw CommandNotFoundException(
                        Context(
                                event = event,
                                prefix = actualPrefix,
                                rawArgs = msg.content.substringAfter(actualPrefix).trimStart(),
                                args = listOf()),
                        cmdCandidateName
                )
        when (cmdCandidate) {
            is CommandGroup -> cmdCandidate.processCommands(
                    event, actualPrefix, msg.content.substringAfter(cmdCandidateName).trimStart()
            )
            is Command -> GlobalScope.launch {
                cmdCandidate.invoke(
                        Context(
                                event = event,
                                prefix = actualPrefix,
                                rawArgs = msg.content.substringAfter(cmdCandidateName).trimStart()
                        )
                )
            }
        }
    }
}
