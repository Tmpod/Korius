/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.core

import com.serebit.strife.data.Color
import com.serebit.strife.entities.Message
import com.serebit.strife.entities.embed
import com.serebit.strife.entities.reply
import com.serebit.strife.entities.author
import dev.tmpod.korius.bot.CustomEmojisConfig
import dev.tmpod.korius.framework.commands.BaseCommand
import org.koin.core.KoinComponent
import org.koin.core.inject

abstract class Cog : KoinComponent {
    open val commands: List<BaseCommand> = listOf()
    open val listeners: List<StrifeEventListener> = listOf()

    /** Adds quick reply methods */
    val emojisConfig: CustomEmojisConfig by inject()

    suspend fun Message.replySuccess(text: String) = reply(
            embed {
                description = "${emojisConfig.check} $text"
                color = Color(3066993)
            }
    )

    suspend fun Message.replyError(text: String) = reply(
            embed {
                description = "${emojisConfig.cross} $text"
                color = Color(16007222)
            }
    )

    suspend fun Message.replyInfo(text: String) = reply(
            embed {
                description = "${emojisConfig.iDot} $text"
                color = Color(2201331)
            }
    )

    suspend fun Message.replyInfo(title: String, text: String) = reply(
            embed {
                author {
                    name = title
                    imgUrl = emojisConfig.iDotURL
                }
                description = text
                color = Color(2201331)
            }
    )
}
