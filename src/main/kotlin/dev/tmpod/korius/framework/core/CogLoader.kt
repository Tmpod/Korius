/*
 *  Copyright © Tmpod 2019
 *  This file is part of Korius.
 *  Korius is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Korius is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Korius. If not, see <https://www.gnu.org/licenses/>.
 */
package dev.tmpod.korius.framework.core

import mu.KLogger
import org.koin.core.KoinComponent
import org.koin.core.inject

class CogLoader : KoinComponent {
    private val logger: KLogger by inject()
    private val strifeEventManager: StrifeEventManager by inject()
    private val commandRegistry: CommandRegistry by inject()

    private fun loadListeners(cog: Cog) = cog.listeners.forEach { strifeEventManager.registerListener(it) }

    private fun loadCommands(cog: Cog) = cog.commands.forEach { commandRegistry.addCommand(it) }

    fun loadCog(cog: Cog) {
        logger.info("Adding cog: `${cog}`")
        loadCommands(cog)
        loadListeners(cog)
    }

    fun loadCogs(vararg cogs: Cog) {
        cogs.forEach { loadCog(it) }
    }
}
