# Kmd

**Kmd** is the command framework built for and used on **[Korius](https://gitlab.com/Tmpod/Korius)**.  
 It aims at providing an easy-to-use commands system that can be used with any Discord API wrapper. Currently, I'm implementing the system with **[Koin](https://insert-koin.io)** and **[Strife](https://gitlab.com/serebit/strife)**.
 
 ### Features
 * Command names and aliases;
 >* Customisable prefixes;
 * Descriptions, briefs *and argument usages*[^1];
 * Easy to use logic DSL;
 * Checks;
 * Comprehensive errors;
 * Useful event system;
 >* Command groups (with check inheritance);
 >* Cooldowns (specific application of checks);

*__Note:__ Items on quotes are yet to be done*

[^1]: This will change if I decide to take another approach at command arguments.

If you have any question or suggestion, please open an issue or contact me via Discord (Tmpod#1933).
